package com.vn.wsdl.configs;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

/**
 * Created by amnn on 4/19/2020.
 */

public class MarshallerConfig {

    @Bean
    @ConditionalOnMissingBean
    public Jaxb2Marshaller marshaller() {
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        String contextPath = "com.vn.wsdl.webservicesserver";
        marshaller.setContextPath(contextPath);
        return marshaller;
    }

    @Bean
    public HttpComponentsMessageSender httpComponentsMessageSender() {
        HttpComponentsMessageSender sender = new HttpComponentsMessageSender();
        sender.setReadTimeout(30000);
        sender.setConnectionTimeout(30000);
        sender.setMaxTotalConnections(1024);

        return sender;
    }

    @Bean
    public SoapConnector soapConnector(Jaxb2Marshaller marshaller) {
        SoapConnector client = new SoapConnector();
        client.setMarshaller(marshaller());
        client.setUnmarshaller(marshaller());
        client.setMessageSender(httpComponentsMessageSender());
        return client;
    }
}
