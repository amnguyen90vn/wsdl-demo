package com.vn.wsdl.configs;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapMessage;

public class SoapConnector extends WebServiceGatewaySupport {
    public Object callWebService(String url, Object request){
//        return getWebServiceTemplate().marshalSendAndReceive(url, request);
        return getWebServiceTemplate().marshalSendAndReceive( url, request,
                webServiceMessage -> {
                    ((SoapMessage)webServiceMessage).setSoapAction(
                            "");
                } );
    }
}
