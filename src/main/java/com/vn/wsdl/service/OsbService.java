package com.vn.wsdl.service;

public interface OsbService {
    public <RP,RQ> RP call(RQ rq, String url);
}
