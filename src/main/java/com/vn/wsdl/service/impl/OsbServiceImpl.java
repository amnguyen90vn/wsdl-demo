package com.vn.wsdl.service.impl;

import com.vn.wsdl.configs.SoapConnector;
import com.vn.wsdl.service.OsbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OsbServiceImpl implements OsbService {

    @Autowired
    SoapConnector soapConnector;

    @Override
    public <RP, RQ> RP call(RQ rq, String url) {
        return (RP)soapConnector.callWebService(url, rq);
    }
}
