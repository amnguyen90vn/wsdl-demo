package com.vn.wsdl;

import com.fasterxml.classmate.TypeResolver;
import com.vn.wsdl.configs.MarshallerConfig;
import com.vn.wsdl.service.OsbService;
import com.vn.wsdl.service.impl.OsbServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@Import({MarshallerConfig.class})
public class WsdlApplication {

    public static void main(String[] args) {
        SpringApplication.run(WsdlApplication.class, args);
    }

    @Bean
    @Primary
    public Docket swaggerApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.any())
                .build()
                .apiInfo(new ApiInfoBuilder().version("1.0").title("wsdl").description("Documentation wsdl services API v" + "1.0").build());
    }

    @Bean
    @ConditionalOnMissingBean
    public OsbService osbService() {
        return new OsbServiceImpl();
    }
}
