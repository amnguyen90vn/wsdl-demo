package com.vn.wsdl.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vn.wsdl.service.OsbService;
import com.vn.wsdl.webservicesserver.NumberToWords;
import com.vn.wsdl.webservicesserver.NumberToWordsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    protected ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    OsbService osbService ;

    @PostMapping("NumberToWords")
    public NumberToWordsResponse NumberToWords(@RequestBody String rq) throws JsonProcessingException {
        NumberToWords request = MAPPER.readValue(rq, new TypeReference<NumberToWords>() {});
        NumberToWordsResponse response = osbService.call(request, "http://www.dataaccess.com/webservicesserver/numberconversion.wso?WSDL");
        System.out.println(response);
        return response;
    }
}
